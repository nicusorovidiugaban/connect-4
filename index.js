// Constants
const ROWS = 6;
const COLS = 7;
const PLAYER_RED = 'red';
const PLAYER_YELLOW = 'yellow';

// Game state
let board = [];
let currentPlayer = PLAYER_RED;
let gameOver = false;
let id = document.getElementById("myDiv");

 // Initialize the board state
function createBoard() {
    for (let row = 0; row < ROWS; row++) {
        board[row] = [];
        for (let col = 0; col < COLS; col++) {
            board[row][col] = null;
        }
    }
}

// Create the game board
function addCircleToBoard() {
    const boardElement = document.querySelector('.board');
    for (let row = 0; row < ROWS; row++) {
        for (let col = 0; col < COLS; col++) {
            const cell = document.createElement('div');
            cell.classList.add('cell', 'empty');
            cell.dataset.row = row;
            cell.dataset.col = col;
            boardElement.appendChild(cell);
        }  
    }
}

// Handle cell click event
function handleCellClick(event) {
    if (gameOver) {
        return;
    }
    const clickedCell = event.target;
    const col = parseInt(clickedCell.dataset.col);
    let row = parseInt(clickedCell.dataset.row);
    let bottomCell;

    // Find the bottommost empty cell in the column
    for (let i = ROWS - 1; i >= 0; i--) {
        const cell = document.querySelector(`.cell[data-row="${i}"][data-col="${col}"]`);
        if (cell.classList.contains('empty')) {
            bottomCell = cell;
            row = i;
            break;
        }
    }

    // If no empty cell found, do nothing
    if (!bottomCell) {
        return;
    }

    // Update the board state
    board[row][col] = currentPlayer;
    bottomCell.classList.remove('empty');
    bottomCell.classList.add(currentPlayer);

    // Check for a win
    if (checkVertically(col) || checkHorizontally(row) || checkPrincipalDiagonally() || checkSecundaryDiagonall()) {
        id.innerHTML = `${currentPlayer.toUpperCase()}` + " " + "Wins";
        gameOver = true;
    } else {
        // Switch players
        currentPlayer = (currentPlayer === PLAYER_YELLOW) ? PLAYER_RED : PLAYER_YELLOW;
    }
}

// Check horizontally 
function checkHorizontally(row) { 
    let count = 0; 
    for (let j = 0; j <= 6; ++j) { 
        if (board[row][j] == currentPlayer) {
            ++count;
        } else {
            count = 0;
        }
        if (count >= 4) {
            return true;
        }
    }
    
    return false;
}  

// Check vertically  
function checkVertically(col) {  
    let count = 0;
    for (let i = 5; i >= 0; --i) {
        if (board[i][col] == currentPlayer) {
            ++count;
        } else {
            count = 0;
        }
        if (count >= 4) {
            return true;
        }
    }  
    return false;
} 

// Check diagonally (top-left to bottom-right)
function checkPrincipalDiagonally() {
    for (let i = 3; i <= 5; ++i) {
        let index = 0, count = 0;
        for (let j = i; j >= 0; --j) {
            if (board[index][j] == currentPlayer) {
                ++count;
            } else {
                count = 0;
            }
            if (count >= 4) {
                return true;
            }
            ++index;
        }
    } 
    
    let extern_index = 0;
    for (let i = 1; i <= 3; ++i) {
        let index = extern_index, count = 0;
        for (let j = 6; j >= i; --j) {
            if (board[index][j] == currentPlayer) {
                ++count;
            } else {
                count = 0;
            }
            if (count >= 4) {
                return true;
            }
            ++index;
        }
        ++extern_index;
    }
    return false;
}

// Check secundary diagonall (top-right to bottom-left)
function checkSecundaryDiagonall() {
    let extern_index = 3, index = 3;
    for (let i = 0; i <= extern_index; ++i) {
        let count = 0;
        for (let j = index; j <= 6; ++j) {
            if (board[i][j] == currentPlayer) {
                ++count;
            } else {
                count = 0;
            }
            if (count >= 4) {
                return true;
            }
        }
        if (extern_index < 5) {
            ++extern_index; 
            --index;
        }
    } 
  
    let externIndex = 4;
    for (let i = 1; i <= 2; ++i) {
        let count = 0, index = i;
        for (let j = 0; j <= externIndex; ++j) {
            if (board[index][j] == currentPlayer) {
                ++count;
            } else {
                count = 0;
            }
            if (count >= 4) {
                return true;
            }
            ++index;
        }
        --externIndex;
    }
    return false;
}

// Start a new game
function newGame() {
    addCircleToBoard();
    const cells = document.querySelectorAll('.cell.empty');
    cells.forEach(cell => cell.addEventListener('click', handleCellClick));
    createBoard();
    currentPlayer = PLAYER_RED;
    gameOver = false;
    cells.forEach(cell => {
        cell.classList.remove(PLAYER_RED, PLAYER_YELLOW);
        cell.classList.add('empty');
     });
 }

 // Start a new game when the page loads
 window.onload = newGame;
